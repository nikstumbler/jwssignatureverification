import com.nimbusds.jose.util.X509CertUtils;
import com.sun.xml.internal.rngom.parse.host.Base;
import sun.misc.BASE64Encoder;
import sun.security.rsa.RSAPublicKeyImpl;

import java.io.*;
import java.math.BigInteger;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

/**
 * https://connect2id.com/products/nimbus-jose-jwt/examples/jws-with-rsa-signature
 *
 * Created by e070769 on 4/17/17.
 */
public class VerifySignature {

    private static PrivateKey privateKey;
    private static PublicKey publicKey;

    public static void main(String[] args) {

        String payload_signed = "{\"timestamp\":\"123445667889\",\"walletId\":\"acmebank\",\"language\":\"en\",\"country\":\"US\",\"redirectURL\":\"testURL.com\"}";

        String payload_verify = "{\"timestamp\":\"123445667889\",\"walletId\":\"acmebank\",\"language\":\"en\",\"country\":\"US\",\"redirectURL\":\"testURL.com\"}";

        getKeyPair();

//        getKeyPairFromKeyStore();

        String publicKeyString = new BigInteger(publicKey.getEncoded()).toString();

//        System.out.println("PUK : "+publicKey);
//        System.out.println("PUK encoded : " + publicKey.getEncoded());
//        System.out.println("keyAsString ( for DB ) : " + Base64.getEncoder().encode(publicKey.getEncoded()));

        System.out.println("-------- --------- ----------");
//        System.out.println("PUK decoded : " + retrievePublicKey( publicKey.getEncoded() ));


        String signature = signPayload(payload_signed, privateKey);

        boolean isVerified = verifySignature(publicKeyString, signature , payload_verify);
        System.out.println(isVerified);

    }

    public static PublicKey retrievePublicKey(byte[] keyStr){

        PublicKey retrievedPublicKey = null;

        X509EncodedKeySpec spec = new X509EncodedKeySpec(keyStr);
        KeyFactory factory;

        try {

            factory = KeyFactory.getInstance("RSA");
            retrievedPublicKey = factory.generatePublic(spec);

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }

        return retrievedPublicKey;

    }

    private static String signPayload(String payload_signed, PrivateKey privateKey) {

        byte[] signature = new byte[0];

        try {

            Signature privateSignature = Signature.getInstance("SHA1withRSA");
            privateSignature.initSign(privateKey);
            privateSignature.update(payload_signed.getBytes());
            signature = privateSignature.sign();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (SignatureException e) {
            e.printStackTrace();
        }
        return Base64.getEncoder().encodeToString(signature);

    }

    private static void getKeyPair() {

        try {

            KeyPairGenerator keyGenerator = KeyPairGenerator.getInstance("RSA");
            keyGenerator.initialize(1024);

            KeyPair kp = keyGenerator.genKeyPair();
            publicKey = (RSAPublicKey)kp.getPublic();
            privateKey = (RSAPrivateKey)kp.getPrivate();

            System.out.println("Private Key : " + privateKey);

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

    }

    public static KeyPair getKeyPairFromKeyStore() {

        KeyStore.PrivateKeyEntry privateKeyEntry = null;
        java.security.cert.Certificate cert = null;

        String keyStorePassword = "masterpass";
        String keyPasswordStr = "masterpass";
        String walletId = "acmebank";

        try {

            File f = new File("src/KeyStore.jks");
            FileInputStream fis = new FileInputStream(f);
            DataInputStream dis = new DataInputStream(fis);
            InputStream ins = dis;

            KeyStore keyStore = KeyStore.getInstance("JCEKS");
            keyStore.load(ins, keyStorePassword.toCharArray());   //Keystore password
            KeyStore.PasswordProtection keyPassword =       //Key password
                    new KeyStore.PasswordProtection(keyPasswordStr.toCharArray());

            privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(walletId, keyPassword);

            cert = keyStore.getCertificate(walletId);

        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (UnrecoverableEntryException e) {
            e.printStackTrace();
        }
        publicKey = cert.getPublicKey();
        privateKey = privateKeyEntry.getPrivateKey();

        return new KeyPair(publicKey, privateKey);
    }

    public static boolean verifySignature(String publicKeyString, String signature, String payload) {

        boolean isVerified = false;

        PublicKey publicKey = retrievePublicKey(  new BigInteger(publicKeyString).toByteArray() );

        try {

            Signature signetcheck = Signature.getInstance("SHA1withRSA");
            signetcheck.initVerify(publicKey);
            signetcheck.update(payload.getBytes());

            byte[] arr = Base64.getDecoder().decode(signature);

            if (signetcheck.verify(arr)) {
                isVerified = true;
            } else {
                isVerified = false;
            }

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (SignatureException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }

        return isVerified;
    }

    public static void generateKeys() {
        try {

            KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");

            SecureRandom secureRandom = new SecureRandom();
//            secureRandom.setSeed(walletId.getBytes());

            generator.initialize(1024,secureRandom);
            KeyPair keyPair = generator.generateKeyPair();

            PrivateKey _privateKey = keyPair.getPrivate();
            PublicKey _publicKey = keyPair.getPublic();

            System.out.println("PrivateKey : " + privateKey);
            System.out.println("PublicKey  : " + publicKey);

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public static byte[] hexStrToBytes(String s) {
        byte[] bytes;
        bytes = new byte[s.length() / 2];
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = (byte) Integer.parseInt(s.substring(2 * i, 2 * i + 2), 16);
        }
        return bytes;
    }
}
