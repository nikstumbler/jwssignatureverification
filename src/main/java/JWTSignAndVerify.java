import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.crypto.RSASSAVerifier;

import java.io.*;
import java.math.BigInteger;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by e070769 on 4/18/17.
 */
public class JWTSignAndVerify {

    private static RSAPrivateKey privateKey;
    private static RSAPublicKey publicKey;

/*
    private static String payload_signed = "{\"timestamp\":\""+ new Date().getTime() +"\",\"walletId\":\"citi\",\"language\":\"en\",\"country\":\"US\",\"redirectURL\":\"testURL.com\"}";

    private static String payload_verify = "{\"timestamp\":\""+ new Date().getTime() +"\",\"walletId\":\"citi\",\"language\":\"en\",\"country\":\"US\",\"redirectURL\":\"testURL.com\"}";
    */

    private static String payload_signed = "{\"timestamp\":\""+ new Date().getTime() +"\",\"walletId\":\"acmebank\",\"language\":\"en\",\"country\":\"US\",\"redirectURL\":\"testURL.com\"}";

    private static String payload_verify = "{\"timestamp\":\""+ new Date().getTime() +"\",\"walletId\":\"acmebank\",\"language\":\"en\",\"country\":\"US\",\"redirectURL\":\"testURL.com\"}";


    public static void main(String[] args) {

        getKeyPairFromKeyStore();

        signverify();

     }

    /**
     * header: new String(Base64.getDecoder().decode("eyJraWQiOiIxMjMiLCJhbGciOiJSUzI1NiJ9"))
     * payload: new String(Base64.getDecoder().decode("eyJ3YWxsZXRJZCI6ImFjbWViYW5rIiwibG9jYWxlIjoiZW5fVVMiLCJyZWRpcmVjdFVSTCI6InRlc3RVUkwifQ"))
     * signature: new String(Base64.getDecoder().decode("JRzWdrozB2d-Pmsl5oqFFNIFHLkNMdYkhqmiLS52BwIV7zevxXFhU-Sg3OrNjPMqnrSkPjqHHK5-gwHnLlYFbfqVTuyNhO2djTeatr3YlmrFL_cMVz6uBPCtkyd6Eh5tGXFd93EMLOak7ncer1vUS9Xb9Uv7nd5s6p_8mjVaM3CsBdn2DP1ZmkY3KWBtY3_rAoP0-gdAPMRm31nd4GigkfXsSpJA2GspN-8DGgUGuUA25CBM8cN7gUlnZop5C5O1YLsBASsEtj-MlO2LbF7DgqAmX0YpPH2he_EtTk2SMirofuajDu8CS6Bbd83yJCVJKfGR3de-Q5yLfQBHD4efxA"))
     */
    private static void signverify() {

        JWSSigner signer = new RSASSASigner(privateKey);

        JWSObject jwsObject = new JWSObject(
                new JWSHeader.Builder(JWSAlgorithm.RS256).keyID("123").build(),
                new Payload(payload_signed));

        try {
            jwsObject.sign(signer);

            String s = jwsObject.serialize();

            System.out.println( "PublicKey : "  + new BigInteger( publicKey.getEncoded() ) );
            System.out.println( "Signature : "  + s);

            jwsObject = JWSObject.parse(s);


            JWSVerifier verifier = new RSASSAVerifier(publicKey);

            System.out.println( jwsObject.verify(verifier) );

        } catch (JOSEException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }


        System.out.println(jwsObject.getPayload().toString().equals(payload_verify));

    }

    public static KeyPair getKeyPairFromKeyStore() {

        KeyStore.PrivateKeyEntry privateKeyEntry = null;
        java.security.cert.Certificate cert = null;

        String keyStorePassword = "masterpass";
        String keyPasswordStr = "masterpass";
        String walletId = "masterpass";

        try {

//            File f = new File("src/KeyStore.jks");
            File f = new File("src/key_store.jks");
            FileInputStream fis = new FileInputStream(f);
            DataInputStream dis = new DataInputStream(fis);
            InputStream ins = dis;

            KeyStore keyStore = KeyStore.getInstance("JCEKS");
            keyStore.load(ins, keyStorePassword.toCharArray());   //Keystore password
            KeyStore.PasswordProtection keyPassword =       //Key password
                    new KeyStore.PasswordProtection(keyPasswordStr.toCharArray());

            privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(walletId, keyPassword);

            cert = keyStore.getCertificate(walletId);

        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (UnrecoverableEntryException e) {
            e.printStackTrace();
        }
        publicKey = (RSAPublicKey) cert.getPublicKey();
        privateKey = (RSAPrivateKey) privateKeyEntry.getPrivateKey();

        return new KeyPair(publicKey, privateKey);
    }
}
